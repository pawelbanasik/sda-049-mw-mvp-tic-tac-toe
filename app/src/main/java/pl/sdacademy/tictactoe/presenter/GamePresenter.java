package pl.sdacademy.tictactoe.presenter;


import pl.sdacademy.tictactoe.model.Board;

public class GamePresenter implements GameContract.Presenter {

    private GameContract.View view;

    private Board board;

    public GamePresenter(GameContract.View view) {
        this.view = view;
        board = new Board();
    }

    @Override
    public void onClick(int location) {
        // sprawdź czy ktoś wygrał
        // jeśli nie, to wyświetl odpowiedni znak
        // i zmodyfikuj stan modelu
    }
}
