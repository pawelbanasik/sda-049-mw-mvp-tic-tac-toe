package pl.sdacademy.tictactoe.presenter;

public interface GameContract {

    interface View {
        void showX(int location);

        void showO(int location);
    }

    interface Presenter {

        void onClick(int location);
    }
}
