package pl.sdacademy.tictactoe;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import pl.sdacademy.tictactoe.presenter.GameContract;
import pl.sdacademy.tictactoe.presenter.GamePresenter;

public class GameActivity extends AppCompatActivity implements GameContract.View {

    private GamePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        presenter = new GamePresenter(this);
    }

    @Override
    public void showX(int location) {
        // wyświetlamy kółko
    }

    @Override
    public void showO(int location) {
        // wyświetlamy krzyżyk
    }
}
